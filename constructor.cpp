#include <bits/stdc++.h>
using namespace std;

class Student
{
public:
    int roll;
    int cls;
    double gpa;
    /// @constructor making as per bellow templates 
    // Student(int r, int c, double g)  //1st method 
    // {
    //     roll = r;
    //     cls = c;
    //     gpa = g;
    // }
    Student(int roll, int cls, double gpa)  //2nd method 
    {
        this->roll = roll; //short form; long form (*this).roll=roll
        this->cls = cls;
        this->gpa = gpa;
    }
};

int main()
{
    Student rahim(29, 10, 4.50);
    Student karim(100,9,4.87);
    cout << rahim.cls <<" " <<rahim.gpa << " " <<rahim.roll << endl;
    cout << karim.cls <<" " <<karim.gpa << " " <<karim.roll << endl;
    return 0;
} 