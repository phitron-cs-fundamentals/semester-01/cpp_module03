#include <bits/stdc++.h>
using namespace std;
//Dynamic Object creation
class Student
{
public:
    int cls;
    int roll;
    double cgpa;
    Student(int roll, int cls, int cgpa)
    {
        this->roll=roll;
        this->cls=cls;
        this->cgpa=cgpa;
    }
};

Student* fun()
{
    // Student rahim(345,5,4.69);
    
    Student *karim = new Student(354, 5, 4.90);
    return karim;
}

int main()
{
    Student* ans=fun();
    // cout << ans.roll << " " << ans.cls << " " << ans.cgpa << endl;
    cout << ans->roll << " " << ans->cls << " " << ans->cgpa << endl;
    delete ans; //deletion the dynamic memory
    // cout << ans->roll << " " << ans->cls << " " << ans->cgpa << endl;

    return 0;
}