#include <bits/stdc++.h>
using namespace std;

class Student
{
public:
    char name[500];
    int roll;
    double cgpa;
};

int main()
{
    Student a;
    cin.getline(a.name, 500);
    cin >> a.roll >> a.cgpa;
    getchar();

    cout << a.name << " " << a.roll << " " << a.cgpa << endl;
    return 0;
}