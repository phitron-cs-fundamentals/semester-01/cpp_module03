#include <bits/stdc++.h>
using namespace std;

class Student
{
public:
    int cls;
    int roll;
    double cgpa;
    Student(int roll, int cls, int cgpa)
    {
        this->roll=roll;
        this->cls=cls;
        this->cgpa=cgpa;
    }
};

Student* fun()
{
    Student rahim(345,5,4.69);
    Student * p = &rahim;
    return p;
}

int main()
{
    Student* ans=fun();
    // cout << ans.roll << " " << ans.cls << " " << ans.cgpa << endl;
    cout << ans->roll << " " << ans->cls << " " << ans->cgpa << endl;
    return 0;
}